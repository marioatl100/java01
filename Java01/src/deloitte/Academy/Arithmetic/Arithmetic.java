package deloitte.Academy.Arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class Arithmetics contains all the artihmetic operations
 * 
 * @author mvillegas
 *
 */
public class Arithmetic {
	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());;

	/**
	 * Function suma asks for two doubles and returns a double value
	 * 
	 * @param a double
	 * @param b double
	 * @return double
	 */
	public static double suma(double a, double b) {
		LOGGER.info("Funcion suma realizada correctamente");
		double c = a + b;
		return c;
	}

	/**
	 * 
	 * Function resta asks for two doubles and returns a double value
	 * 
	 * @param a double
	 * @param b double
	 * @return double
	 */
	public static double resta(double a, double b) {
		LOGGER.info("Funcion resta realizada correctamente");
		double c = a - b;
		return c;
	}

	/**
	 * Multiplies all the values of the array and returns the result
	 * 
	 * @param a double[] array
	 * @return double
	 */
	public static double multiplica(double[] a) {
		double c = 0;
		LOGGER.info("Funcion multiplica realizada correctamente");
		for (int i = 0; i < a.length; i++) {
			c = c * a[i];
		}
		return c;
	}

	/**
	 * Divide the two provided values and returns the result
	 * 
	 * @param a double
	 * @param b double
	 * @return double
	 */
	public static double divide(double a, double b) {
		double c = 0;
		try {
			c = a / b;
			LOGGER.info("Funcion divide realizada correctamente");
			return c;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());
			return c;
		}

	}

	/**
	 * Function modulo returns the modulus result between two doubles
	 * 
	 * @param a double
	 * @param b double
	 * @return double
	 */
	public static double modulo(double a, double b) {
		double c = 0;
		try {
			LOGGER.info("Funcion modulo realizada correctamente");
			c = a % b;
			return c;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			return c;

		}
	}
}
