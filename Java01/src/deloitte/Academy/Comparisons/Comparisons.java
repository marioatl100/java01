package deloitte.Academy.Comparisons;

import java.util.logging.Logger;

/**
 * Comparison class contains method to compare int, string and char values
 */

public class Comparisons {

	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * Compares two ints and returns true if they are the same
	 * 
	 * @param a int
	 * @param b int
	 * @return boolean
	 */
	public static boolean esIgualNum(int a, int b) {
		if (a == b) {
			LOGGER.info("Funcion esIgualNum realizada correctamente");
			return true;
		}
		return false;
	}

	/**
	 * Compares two Strings and returns true if they are equal
	 * 
	 * @param a String
	 * @param b String
	 * @return boolean
	 */
	public static boolean esIgualStrings(String a, String b) {

		if (a.contentEquals(b)) {
			LOGGER.info("Funcion esIgualStrings realizada correctamente");
			return true;
		}
		return false;
	}

	/**
	 * compares two chars and returns true if they are the same
	 * 
	 * @param a char
	 * @param b char
	 * @return boolean
	 */
	public static boolean esIgualChar(char a, char b) {
		if (a == b) {
			LOGGER.info("Funcion esIgualChar realizada correctamente");
			return true;
		}
		return false;
	}

}
