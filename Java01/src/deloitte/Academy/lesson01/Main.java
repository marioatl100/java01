package deloitte.Academy.lesson01;

import deloitte.Academy.Arithmetic.Arithmetic;
import deloitte.Academy.Comparisons.Comparisons;
import deloitte.Acadeny.LogicalOperators.LogicalOperators;

/**
 * 
 * @author mvillegas Clase Main Ejecuta todos los metodos creados
 */

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] array = { 1, 2, 3 };
		Arithmetic.suma(4.0, 5.0);
		System.out.println(Arithmetic.divide(5, 0));
		Arithmetic.modulo(43, 5);
		Arithmetic.resta(7, 4);
		Arithmetic.multiplica(array);
		Comparisons.esIgualChar('b', 'c');
		Comparisons.esIgualNum(4, 4);
		Comparisons.esIgualStrings("Hola", "Adios");
		LogicalOperators.dosNegativos(-3, -6);
		LogicalOperators.unNegativos(2, 4);

	}

}
