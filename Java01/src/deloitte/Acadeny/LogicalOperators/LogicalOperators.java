package deloitte.Acadeny.LogicalOperators;

import java.util.logging.Logger;

/**
 * Logical Operator class contains 2 identification methods methods
 * 
 * @author mvillegas
 *
 */

public class LogicalOperators {
	private static final Logger LOGGER = Logger.getLogger(LogicalOperators.class.getName());

	//
	/**
	 * Function dosNegativos returns true if the 2 asked numbers are negative
	 * 
	 * @param a: int
	 * @param b: int
	 * @return: boolean
	 */
	public static boolean dosNegativos(int a, int b) {
		if (a < 0 && b < 0) {
			LOGGER.info("Funcion dosNegativos realizada correctamente");
			return true;
		}
		return false;
	}

	//
	/**
	 * Function dosNegativos returns true if 1 of the asked numbers is negative
	 * 
	 * @param a: int
	 * @param b: int
	 * @return: boolean
	 */
	public static boolean unNegativos(int a, int b) {
		if (a < 0 && b < 0) {
			LOGGER.info("Funcion unNegativo realizada correctamente");
			return true;
		}
		return false;
	}
}
